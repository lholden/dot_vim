call plug#begin($VIMHOME . '/plugins')

""" Basics
Plug 'Shougo/vimproc.vim'
Plug 'tpope/vim-sensible'

""" General plugins
Plug 'Shougo/unite.vim'
Plug 'Shougo/vimfiler.vim'
Plug 'vim-airline/vim-airline'
Plug 'easymotion/vim-easymotion'
Plug 'mhinz/vim-signify'
Plug 'vits/ZoomWin'
Plug 'talek/obvious-resize'
Plug 'sickill/vim-pasta'
Plug 'tpope/vim-unimpaired'

""" Languages
Plug 'kchmck/vim-coffee-script'
Plug 'AndrewRadev/vim-eco'

""" Themes
Plug 'nanotech/jellybeans.vim'

"Plug 'ryanoasis/vim-devicons'
call plug#end()
